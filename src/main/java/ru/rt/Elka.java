package ru.rt;

import java.util.Scanner;

public class Elka {
    public Elka(){
        int i,j,n;
        System.out.println("Введите данные для задачи Цифровая ёлочка:");
        System.out.println("n:");
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        int[][] tree = new int[n][n];
        System.out.println("tree:");
        for (i = 0; i < n; i++) {
            for (j = 0; j <= i; j++) {
                tree[i][j] = sc.nextInt();
            }
        }

        for (i = n-2; i >= 0; i--) {
            for (j = 0; j <= i; j++) {
                tree[i][j] += Math.max(tree[i+1][j],tree[i+1][j+1]);
            }
        }
        System.out.println(tree[0][0]);
    }
}
