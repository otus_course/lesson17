package ru.rt;

import java.util.Scanner;

public class Razdvagoroh {

    public Razdvagoroh(){
        System.out.println("Введите данные для задачи Раз/два горох:");
        Scanner sc = new Scanner(System.in);
        String[] s = sc.nextLine().split("\\+|/");
        int a,b,c,d,x,y,nod;
        a = Integer.parseInt(s[0]);
        b = Integer.parseInt(s[1]);
        c = Integer.parseInt(s[2]);
        d = Integer.parseInt(s[3]);
        x = a*d + b*c;
        y = b*d;
        nod = NOD(x,y);
        x/=nod;
        y/=nod;
        System.out.println(x+"/"+y);
    }

    private boolean even (int num){
        return (num & 1) == 0;
    }
    private boolean odd (int num){
        return (num & 1) == 1;
    }

    private int NOD(int a, int b) {
        if(a==b) return a;
        if(a==0) return b;
        if(b==0) return a;
        if(even(a) && even(b)) return NOD(a>>1,b>>1) << 1;
        if(even(a) && odd(b)) return NOD(a>>1,b);
        if(odd(a) && even(b)) return NOD(a,b>>1);
        if(a>b) return NOD((a-b)>>1,b);
        return NOD(a, (b-a)>>1);
    }
}
