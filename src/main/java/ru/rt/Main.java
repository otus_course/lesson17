package ru.rt;

import java.util.Arrays;

public class Main {


    public static void main(String[] args) throws Exception {
        new Razdvagoroh();
//        new Elka();
//        new Pjatjuvosem();
//        new Ostrova();
//        sarai();
    }

    public static void sarai(){
        int[] Ns = {10,1000,2000,5000,10000,25000};
        int[] Fs = {10,20,50};

        for (int N : Ns) {
            for (int F : Fs) {
                Sarai s = new Sarai(N,N,F,200500);
                long time = System.currentTimeMillis();
                int maxSq = s.start3();
                time = System.currentTimeMillis() - time;
                System.out.println("size: " + N+":" + F + "\tAnswer " + maxSq + "\tTime " + time + " ms.");
            }
        }
    }
}
