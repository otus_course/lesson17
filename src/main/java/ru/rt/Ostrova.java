package ru.rt;

import java.util.Scanner;

public class Ostrova {
    int n;
    int[][] map;

    public Ostrova() {
        int i,j;
        System.out.println("Введите данные для задачи Острова:");
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        map = new int[n][n];
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                map[i][j] = sc.nextInt();
            }
        }
        int islands = 0;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if(map[i][j] == 1){
                    islands++;
                    walk(i,j);
                }
            }
        }
        System.out.println(islands);
    }

    private void walk(int x, int y) {
        if(x < 0 || x >=n) return;
        if(y < 0 || y >=n) return;
        if(map[x][y] == 0) return;
        map[x][y] = 0;
        walk(x-1,y);
        walk(x+1,y);
        walk(x,y-1);
        walk(x,y+1);
    }
}
